package test;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.ExcludeTags;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

// tags: addValues, subValues, TestSuite1, TestSuite2, TestSuite2
// tags: tdd, multiplyValues, powValues, divValues, sqrtValues, Smoketests

// tell the compiler to run tests with JUnit Platform instead JUnit Jupiter
@RunWith(JUnitPlatform.class)
// tell the compiler from which classes tests shall be run
// here we picked two classes
@SelectClasses({CalcAppTest.class, SecondTest.class})
// for one class we can omit the curly brackets
//@SelectClasses(CalcAppTest.class)

// right now all the tests from selected classes will be executed
// we can change this with IncludeTags and ExcludeTags
// uncomment IncludeTags, to run only tests that are tagged with selected tags
//@IncludeTags({"addValues", "TestSuite2"})
// uncomment ExcludeTags, to run all tests except of those tagged with selected tags
//@ExcludeTags({"addValues"})
// uncommenting both will result in running tests that have at least one of included tags, but
// in the same time doesn't have any of excluded tags

// just run the test class after commenting/uncommenting above annotations and changing tags
// you will see the number of executed tests is changing
public class SelectedTests {
}
